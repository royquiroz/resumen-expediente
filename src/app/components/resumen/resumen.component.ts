import { Component, OnInit } from "@angular/core";
import { ExpedienteService } from "../../services/expediente.service";

@Component({
  selector: "app-resumen",
  templateUrl: "./resumen.component.html"
})
export class ResumenComponent implements OnInit {
  error: any;
  data: any;
  loading: boolean = false;
  constructor(private _expedienteService: ExpedienteService) {}

  ngOnInit() {
    this.searchData();
  }

  searchData() {
    this._expedienteService.data().subscribe(
      (res: any) => {
        this.transformObjectData(res);
        console.log(this.data.expedientes);
      },
      err => {
        console.log(err);
        this.error = err;
      }
    );
  }

  transformObjectData(data) {
    var array = [];
    for (let key in data) {
      array.push(data[key][0]);
    }

    var newData = {};
    array.forEach(e => {
      for (let key in e) {
        const element = e[key];
        newData[key] = element;
      }
    });
    this.data = newData;
    this.loading = true;
  }
}
