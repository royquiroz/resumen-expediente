import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { map, catchError } from "rxjs/operators";
import { Observable, throwError } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ExpedienteService {
  expedienteUrl =
    "http://minotaria.net/base/_pruebas/db_export.php?ver=todo&exp=15/1/1";

  constructor(private http: HttpClient) {
    console.log("Expediente Service Listo");
  }

  data(): Observable<any> {
    return this.http.get(this.expedienteUrl).pipe(
      map((data: any) => {
        return data[0];
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }
}
